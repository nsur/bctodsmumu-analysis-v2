// -*- C++ -*-
//
// Package:    Test/BcToDsMuMuNtuplizer
// Class:      BcToDsMuMuNtuplizer
//
/**\class BcToDsMuMuNtuplizer BcToDsMuMuNtuplizer.cc Test/BcToDsMuMuNtuplizer/plugins/BcToDsMuMuNtuplizer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Nairit Sur
//         Created:  Wed, 21 Oct 2020 17:15:34 GMT
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
//#include "DataFormats/TrackReco/interface/Track.h"
//#include "DataFormats/TrackReco/interface/TrackFwd.h"
 
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "RecoVertex/KinematicFitPrimitives/interface/ParticleMass.h"

#include "TTree.h"
#include <TLorentzVector.h>

using namespace std;
using namespace reco;
using namespace edm;

struct HistArgs{
  char name[128];
  char title[128];
  int n_bins;
  double x_min;
  double x_max;
};

enum HistName{
  h_pt,
  h_eta,
  h_trkmass,
  nHistNameSize
};

HistArgs hist_args[nHistNameSize] = {
  // name, title, n_bins, x_min, x_max
  {"h_pt" , "Track p_{T}; p_{T} [GeV]" , 1000 , 0 , 100},
  {"h_eta" , "Track #eta; #eta" , 1000 , -3. , 3.},
  {"h_trkmass", "Three track invariant mass; m_{KK#pi} [GeV]", 1000, 0, 100},
};

TH1D *histos[nHistNameSize];

//
// class declaration
//

// If the analyzer does not use TFileService, please remove
// the template argument to the base class so the class inherits
// from  edm::one::EDAnalyzer<>
// This will improve performance in multithreaded jobs.


using reco::TrackCollection;

class BcToDsMuMuNtuplizer : public edm::one::EDAnalyzer<edm::one::SharedResources>  {
   public:
      explicit BcToDsMuMuNtuplizer(const edm::ParameterSet&);
      ~BcToDsMuMuNtuplizer();

      static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);


   private:
      virtual void beginJob() override;
      virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
      virtual void endJob() override;
      
      void clearVariables();

      // ----------member data ---------------------------
      
      //edm::EDGetTokenT<TrackCollection> tracksToken_;  //used to select what tracks to read from configuration file
      
      edm::EDGetTokenT<edm::View<pat::PackedCandidate>> tracksToken_;
      
      ParticleMass KaonMass_;
      
      //pre-selection cuts
      double TrkMinPt_;
      double TrkMaxEta_;
      
      TTree* tree_;
      vector<double>  *trk1px, *trk1py, *trk1pz, *trk1chg;
      vector<double>  *trk2px, *trk2py, *trk2pz, *trk2chg;
      
      
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
BcToDsMuMuNtuplizer::BcToDsMuMuNtuplizer(const edm::ParameterSet& iConfig):
  //tracksToken_(consumes<TrackCollection>(iConfig.getUntrackedParameter<edm::InputTag>("tracks")))
  tracksToken_(consumes<edm::View<pat::PackedCandidate>>(iConfig.getParameter<edm::InputTag>("tracks"))),
  
  KaonMass_(iConfig.getUntrackedParameter<double>("KaonMass")),
  
  //pre-selection cuts
  TrkMinPt_(iConfig.getUntrackedParameter<double>("TrkMinPt")),
  TrkMaxEta_(iConfig.getUntrackedParameter<double>("TrkMaxEta")),
  
  tree_(0),
  trk1px(0), trk1py(0), trk1pz(0), trk1chg(0),
  trk2px(0), trk2py(0), trk2pz(0), trk2chg(0)
{
   //now do what ever initialization is needed

}


BcToDsMuMuNtuplizer::~BcToDsMuMuNtuplizer()
{

   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called for each event  ------------
void
BcToDsMuMuNtuplizer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  edm::Handle< View<pat::PackedCandidate> > tracks;
  iEvent.getByToken(tracksToken_,tracks);

  std::cout<< " number of tracks:   " << tracks->size() << endl;
  //LogInfo("Demo") << "number of tracks "<<tracks->size();
  int nTrack = 0;

  for(View<pat::PackedCandidate>::const_iterator itTrack1 = tracks->begin(); itTrack1 != tracks->end(); ++itTrack1 ){ //track 1 loop
    
    if (itTrack1->pt() < TrkMinPt_)  continue;
    if (fabs(itTrack1->eta()) > TrkMaxEta_)  continue;
    if (itTrack1->charge() == 0)  continue;
    nTrack++;
    
    for(View<pat::PackedCandidate>::const_iterator itTrack2 = itTrack1+1; itTrack2 != tracks->end(); ++itTrack2 ){ //track 2 loop
    
      if (itTrack2->pt() < TrkMinPt_)  continue;
      if (fabs(itTrack2->eta()) > TrkMaxEta_)  continue;
      if (itTrack1->charge() * itTrack2->charge() >= 0)  continue;
      
      TLorentzVector K1_4V, K2_4V, Ds_4V;
      
      K1_4V.SetXYZM(itTrack1->px(),itTrack1->py(),itTrack1->pz(),KaonMass_);
      K2_4V.SetXYZM(itTrack2->px(),itTrack2->py(),itTrack2->pz(),KaonMass_);
      
      Ds_4V = K1_4V + K2_4V;
      
      histos[h_trkmass]->Fill(Ds_4V.M());
    
      histos[h_pt]->Fill(itTrack1->pt());
      histos[h_eta]->Fill(itTrack1->eta());
      
      histos[h_pt]->Fill(itTrack2->pt());
      histos[h_eta]->Fill(itTrack2->eta());
    
      trk1px->push_back(itTrack1->px());
      trk1py->push_back(itTrack1->py());
      trk1pz->push_back(itTrack1->pz());
      trk1chg->push_back(itTrack1->charge());
      
      trk2px->push_back(itTrack2->px());
      trk2py->push_back(itTrack2->py());
      trk2pz->push_back(itTrack2->pz());
      trk2chg->push_back(itTrack2->charge());
    
    } // track 2 loop
  } //track 1 loop
  std::cout<< " number of good tracks:   " << nTrack << endl;

#ifdef THIS_IS_AN_EVENT_EXAMPLE
   Handle<ExampleData> pIn;
   iEvent.getByLabel("example",pIn);
#endif

#ifdef THIS_IS_AN_EVENTSETUP_EXAMPLE
   ESHandle<SetupData> pSetup;
   iSetup.get<SetupRecord>().get(pSetup);
#endif

  tree_->Fill();
  clearVariables();
}


// ------------ method called once each job just before starting event loop  ------------
void
BcToDsMuMuNtuplizer::beginJob()
{
  edm::Service<TFileService> fs;
  
  tree_ = fs->make<TTree>("BcToDsMuMuTree", "BcToDsMuMu Tree");
  
  tree_->Branch("trk1px", &trk1px);
  tree_->Branch("trk1py", &trk1py);
  tree_->Branch("trk1pz", &trk1pz);
  tree_->Branch("trk1chg", &trk1chg);
  
  tree_->Branch("trk2px", &trk2px);
  tree_->Branch("trk2py", &trk2py);
  tree_->Branch("trk2pz", &trk2pz);
  tree_->Branch("trk2chg", &trk2chg);
  
  for(int i=0; i<nHistNameSize; i++) {
    histos[i] = fs->make<TH1D>(hist_args[i].name, hist_args[i].title, hist_args[i].n_bins,hist_args[i].x_min, hist_args[i].x_max);
  }

}

// ------------ method called once each job just after ending the event loop  ------------
void
BcToDsMuMuNtuplizer::endJob()
{

  tree_->GetDirectory()->cd();
  tree_->Write();
//  for(int i = 0; i < nHistNameSize; i++) {
//    histos[i]->Write();
//    histos[i]->Delete();
//  }


}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
BcToDsMuMuNtuplizer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

  //Specify that only 'tracks' is allowed
  //To use, remove the default given above and uncomment below
  //ParameterSetDescription desc;
  //desc.addUntracked<edm::InputTag>("tracks","ctfWithMaterialTracks");
  //descriptions.addDefault(desc);
}

void
BcToDsMuMuNtuplizer::clearVariables() {
  trk1px->clear(); trk1py->clear(); trk1pz->clear(); trk1chg->clear();
  trk2px->clear(); trk2py->clear(); trk2pz->clear(); trk2chg->clear();
}

//define this as a plug-in
DEFINE_FWK_MODULE(BcToDsMuMuNtuplizer);
