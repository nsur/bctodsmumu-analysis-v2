import FWCore.ParameterSet.Config as cms

#process = cms.Process("Demo")
process = cms.Process("Ntuple")

process.load("FWCore.MessageService.MessageLogger_cfi")
process.load("Configuration.StandardSequences.MagneticField_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
process.load("Configuration.StandardSequences.GeometryRecoDB_cff")
process.load("Configuration.StandardSequences.Reconstruction_cff")
process.GlobalTag.globaltag = cms.string("102X_dataRun2_v12")

process.MessageLogger.cerr.FwkReport.reportEvery = 10
process.options = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(1000) )

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
        #'file:myfile.root'
        #'/store/data/Run2016F/Charmonium/MINIAOD/17Jul2018-v1/50000/F48B20B4-498B-E811-BDC7-0CC47A4D9A50.root'
        "/store/data/Run2016F/DoubleMuonLowMass/MINIAOD/17Jul2018-v1/40000/10AEEFA3-7D8C-E811-9F2E-0CC47A544E12.root"
    )
)

#taskBc = cms.Task()
#process.load("PhysicsTools.PatAlgos.slimming.unpackedTracksAndVertices_cfi")
#taskBc.add(process.unpackedTracksAndVertices)


process.ntuple = cms.EDAnalyzer("BcToDsMuMuNtuplizer",
    #tracks = cms.untracked.InputTag('ctfWithMaterialTracks')
    #tracks = cms.untracked.InputTag('unpackedTracksAndVertices')
    KaonMass = cms.untracked.double(0.493677),
    KaonMassErr = cms.untracked.double(1.6e-5),
    
    tracks = cms.InputTag("packedPFCandidates"),
    
    TrkMinPt = cms.untracked.double(0.8),
    TrkMaxEta = cms.untracked.double(2.5)
)

process.TFileService = cms.Service("TFileService",
    fileName = cms.string("BcToDsMuMuNtuple.root")
)

process.p = cms.Path(process.ntuple)
